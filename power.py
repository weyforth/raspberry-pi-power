import sys
import time
import RPi.GPIO as GPIO
from subprocess import call

exit = False
powerBtnPin = 25
sensePin = 24
rebooting = False
poweringDown = False
btnTimeStart = 0

def power_button_event(pin):
	if GPIO.input(powerBtnPin) == 1:
		power_button_pressed()
	else:
		power_button_released()

def power_button_pressed():
	global rebooting

	btnTimeStart = time.time()

	while GPIO.input(powerBtnPin) == 1:
		if time.time() - btnTimeStart >= 2.0 and not rebooting and not poweringDown:
			print 'Rebooting'
			rebooting = True
			call(['sudo', 'reboot'])

def power_button_released():
	global rebooting, poweringDown

	if not rebooting and not poweringDown:
		poweringDown = True
		print 'Powering Down'
		call(['sudo', 'shutdown', '-P', '-h', 'now'])

try:

	GPIO.setmode(GPIO.BCM)
	GPIO.setup(sensePin, GPIO.OUT)
	GPIO.output(sensePin, 1)
	GPIO.setup(powerBtnPin, GPIO.IN)
	GPIO.add_event_detect(powerBtnPin,GPIO.BOTH,callback=power_button_event)

	while True:
		time.sleep(1)
		

except KeyboardInterrupt:  

	# Keyboard Interrupt
	exit = True

except:

	# Any other error
	print sys.exc_info()[0]
	exit = True

finally:

	GPIO.cleanup();
